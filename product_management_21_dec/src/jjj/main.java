package jjj;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {
	static  Product product;
	static List<Product> productList = new ArrayList();
	static ArrayList<String> li = new ArrayList<>();
	static Connection con;
	static Statement stmt;
	
	public static void main(String[] args) throws IOException, SQLException {
		int optionNumber =0;
		Scanner sc= new Scanner(System.in);
		System.out.println("** WELCOME TO PRODUCT MANAGEMENT **");
		createDbConnection();
		//loadProductList();
		while(optionNumber!=5) {
			showOption();
			System.out.println("Enter your choice :");
			optionNumber =sc.nextInt();
			if(optionNumber == 1) {
				addProduct();
			}else if(optionNumber==2) {
				updateProduct();
			}
			else if(optionNumber==3){
				 searchProduct();
			}
			else if(optionNumber==4) {
				deleteProduct();
			}else if(optionNumber==5) {
				//writeArrayListToFile();
				System.out.println("Exit Application");
				con.close();
				System.exit(0);
				
			}else {
				System.out.println("Invalid Option");
		}
	}
	}
public static void showOption() {
	System.out.println("\t Choose Operation:");
	System.out.println("\t 1.Add Product");
	System.out.println("\t 2.Update Product");
	System.out.println("\t 3.Search Product");
	System.out.println("\t 4.Delete Product");
	System.out.println("\t 5.Exit");
}

  public static void addProduct() throws IOException {
	  System.out.println("***** ADD PRODUCT *****\n\n");
	  Scanner sc = new Scanner(System.in);
	  System.out.println("Enter Product Name:");
	  String pname = sc.nextLine();
	  System.out.println("Enter Product Quantity:");
	  String quantity = sc.nextLine();
	  System.out.println("Enter Product price:");
	  String price = sc.nextLine();
	 
	  li.add(pname+","+price+","+quantity);
	  try {
			boolean rs1= stmt.execute("insert into Products (id, Name, Quantity, price) values (11 ,'"+ pname+"', '"+price+"','"+quantity+"');");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
	 
  }
  public static void updateProduct() throws IOException {
	  System.out.println("***** UPDATE PRODUCT *****\n\n");
	  Scanner sc = new Scanner(System.in);
	  System.out.println("Enter Product Name which You Want To Update:");
	  String productNameToUpdate = sc.nextLine();
	  System.out.println("You are Updating Product"+productNameToUpdate);
	  System.out.println("Enter Product Name:");
	  String pname = sc.nextLine();
	  System.out.println("Enter Product Quantity:");
	  String quantity = sc.nextLine();
	  System.out.println("Enter Product price:");
	  String price = sc.nextLine();
//	  String path = "C:\\Users\\Admin\\eclipse-workspace\\ProductManagement25Nov\\src\\Product";
//	  File file = new File(path);
//	  FileWriter fw = new FileWriter(file);
//	  BufferedWriter bw = new BufferedWriter(fw);
	  try {
			boolean rs1= stmt.execute("update products set Name ='"+pname+"',price ='"+price+"',quantity ='"+quantity+"' where Name='"+productNameToUpdate+"';");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
  
//	  bw.write(pname+","+price+","+quantity);
//	  li.set(getIndexOf(productNameToUpdate), pname+","+price+","+quantity);
//	  bw.close();
//	  fw.close();
  }
  public static void deleteProduct() {
	  System.out.println("**** DELETE PRODUCT ****");
	  System.out.println("Enter the Product name Which You Want to Delete:");
	  Scanner sc = new Scanner (System.in);
	  String productNameToDelete = sc.nextLine();
	  try {
			boolean rs1= stmt.execute(" delete from products where name ='"+productNameToDelete+"'");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
	 // li.remove(getIndexOf(productNameToDelete));
  }
  public static void searchProduct() {
	  System.out.println("*** SEARCH PRODUCT ***");
	  System.out.println("*** Enter Product Name Which You Want to Search:");
	  Scanner sc = new Scanner(System.in);
	  
	  String productNameWantToSearch= sc.nextLine();
	  try {
			boolean rs1= stmt.execute("Search product from products where ='"+productNameWantToSearch+"'");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
	  li.get(getIndexOf( productNameWantToSearch));
	  
  }
  
  public static int getIndexOf(String itemName) {
	  int i= 0;
	  for(String s:li) {
		  if(s.contains(itemName)) {
			  return i;
		  }
		  i++;  
	  }
	return -1;
  }
  
 
  public static void createDbConnection() {
	
  try {
		 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/product_management_1","root","@Sagar05");
	     stmt =con.createStatement();
  }catch (Exception e)
	{
		System.out.println(e);
	} 
}
}