package product_management_21_dec;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main1 {

	static Connection con;
	static Statement stmt;
	
	public static void main(String[] args) throws IOException, SQLException {
		int optionNumber =0;
		Scanner sc= new Scanner(System.in);
		System.out.println("** WELCOME TO PRODUCT MANAGEMENT **");
		createDbConnection();
		while(optionNumber!=5) {
			showOption();
			System.out.println("Enter your choice :");
			optionNumber =sc.nextInt();
			if(optionNumber == 1) {
				addProduct();
			}else if(optionNumber==2) {
				updateProduct();
			}
			else if(optionNumber==3){
				 searchProduct();
			}
			else if(optionNumber==4) {
				deleteProduct();
			}else if(optionNumber==5) {
				System.out.println("Exit Application");
				con.close();
				System.exit(0);
			}
			else {
				System.out.println("Invalid Option");
		}
	}
	}
public static void showOption() {
	System.out.println("\t Choose Operation:");
	System.out.println("\t 1.Add Product");
	System.out.println("\t 2.Update Product");
	System.out.println("\t 3.Search Product");
	System.out.println("\t 4.Delete Product");
	System.out.println("\t 5.Exit");
}

  public static void addProduct() throws IOException {
	  System.out.println("***** ADD PRODUCT *****\n\n");
	  Scanner sc = new Scanner(System.in);
	  System.out.println("Enter Product Name:");
	  String pname = sc.nextLine();
	  System.out.println("Enter Product Quantity:");
	  String quantity = sc.nextLine();
	  System.out.println("Enter Product price:");
	  String price = sc.nextLine();
	 
	  try {
		  
			boolean rs1= stmt.execute("insert into Product(Id, Name, Quantity, Price) values (2 ,'"+ pname+"', '"+price+"','"+quantity+"');");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
	 
  }
  public static void updateProduct() throws IOException {
	  System.out.println("***** UPDATE PRODUCT *****\n\n");
	  Scanner sc = new Scanner(System.in);
	  System.out.println("Enter Product Name which You Want To Update:");
	  String productNameToUpdate = sc.nextLine();
	  System.out.println("You are Updating Product"+productNameToUpdate);
	  System.out.println("Enter Product Name:");
	  String pname = sc.nextLine();
	  System.out.println("Enter Product Quantity:");
	  String quantity = sc.nextLine();
	  System.out.println("Enter Product price:");
	  String price = sc.nextLine();
	  try {
			boolean rs1= stmt.execute("update Product set Name ='"+pname+"',Price ='"+price+"',Quantity ='"+quantity+"' where Name='"+productNameToUpdate+"';");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
  }
  public static void deleteProduct() {
	  System.out.println("**** DELETE PRODUCT ****");
	  System.out.println("Enter the Product name Which You Want to Delete:");
	  Scanner sc = new Scanner (System.in);
	  String productNameToDelete = sc.nextLine();
	  try {
			boolean rs1= stmt.execute(" delete from Product where Name ='"+productNameToDelete+"'");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
	 
  }
  public static void searchProduct() {
	  System.out.println("*** SEARCH PRODUCT ***");
	  System.out.println("*** Enter Product Name Which You Want to Search:");
	  Scanner sc = new Scanner(System.in);
	  
	  String productNameWantToSearch= sc.nextLine();
	  try {
			boolean rs1= stmt.execute("Select Name from Product where ='"+productNameWantToSearch+"'");
			
		} catch (Exception e)
		{
			System.out.println(e);
		} 
  }
 
 
  public static void createDbConnection() {
	
  try {
		 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/product_management1","root","@Sagar05");
	     stmt =con.createStatement();
  }catch (Exception e)
	{
		System.out.println(e);
	} 
}




}
